package com.rupak.springcloud.guestservice.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="guestservice")
public class Configuration {

	@Setter
	@Getter
	private String name;

	@Setter
	@Getter
	private String description;

}
