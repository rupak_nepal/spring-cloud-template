package com.rupak.springcloud.reservationservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ReservationservicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReservationservicesApplication.class, args);
    }

}
